This module allows for calculating momentum dependent operators given a Hamiltonian defined as a string, a SymPy expression or an infinite kwant Builder. The eigen-properties are calculated as well as the momentum dependent velocities.
Subsequently the spin density, current density or Berry curvature 
on a given region of the momentum space can be computed. 