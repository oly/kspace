from math import e
import itertools as it
import collections
import numpy as np
import tinyarray as ta
import sympy
from sympy import Matrix, lambdify, sympify, Symbol
import kwant
from kwant import qsymm

__all__ = ['Density', 'Current', 'BerryCurvature', 'k_system']

class _Kspace:
    """Base class for calculating k-space proprties of a
    momentum dependent Hamiltonian.
    Attributes :
    -----------
    hamiltonian : str, SymPy expression or infinite Builder
        The k-space hamiltonian matrix.
    locals : dict, optional, default (None)
        The parameters used in the definition of `hamiltonian`.
    k_1 : list or 1d numpy array
        the momentum values in the first dimension.
    k_2 : list or 1d numpy array
        the momentum values in the second dimension.
    k_3 : list or 1d numpy array
        the momentum values in the third dimension.
    zone : callable, optional, default (None)
        The zone of the momentum space or the Brillouin zone.

    Methods :
    --------
    velocity :
        Iterates over the momentum vectors and calculates
        the velocity along a given direction.
    eigen_propreties :
        Iterates over the momentum vectors and calculates
        the eigen values and eigen functions.
    current_operator :
        Iterates over the momentum vectors and calculates
        the current density operator.
    """
    def __init__(self, hamiltonian, locals=None, k_1=None, k_2=None, k_3=None, zone=None):
        self.hamiltonian = hamiltonian
        self.zone = zone
        self.locals = locals
        self.k_1 = k_1
        self.k_2 = k_2
        self.k_3 = k_3
        ensure_is_str_sympy_or_builder(self.hamiltonian)
        assert isinstance(self.k_1, (list, np.ndarray)), \
               "the momentum space should be at least 1d"
        # Lambdify hamiltonian and get its size (norbs)
        _symbolic = Symbolic(self.hamiltonian, self.locals)
        self.H = _symbolic._lambdify()
        self.norbs = _symbolic.norbs
        # Make symbolic velocities along the available directions
        self.velocities = _symbolic.make_velocities()
        # Get the momentum vectors within `zone`
        self.k_vectors = _momentum_vectors(self.k_1, self.k_2, self.k_3, zone=self.zone)

    def velocity(self, direction):
        r"""
        Gives the volocity :
            `:math: \partial_{k_i} H`
        along the direction i. If a list or a tuple of floats is given
        a linear combination of velocities is considered.
        Then the elements of `direction` give the respective
        coefficients along x, y and z.
        """
        if isinstance(direction, str):
            for k in self.k_vectors:
                yield self.velocities[direction](*k)
        elif isinstance(direction, (list, tuple)):
            for k in self.k_vectors:
                _sum = 0
                for (_axis, (_coord, velo)) in enumerate(self.velocities.items()):
                    _sum += direction[_axis] * velo(*k)
                yield _sum
        else:
            raise TypeError("direction should be a string from 'xyz', a list or tuple of floats")

    def eigen_properties(self):
        # The eigen properties are given in ascending order
        for k in self.k_vectors:
            yield np.linalg.eigh(self.H(*k))

    def current_operator(self, pauli, direction='x'):
        # Gives the current density matrix J_i^j = 1/2 {\sigma_i, v_j}
        for v in self.velocity(direction):
            yield  0.5 * (ta.dot(v, pauli) + ta.dot(pauli, v))

class Density(_Kspace):
    def __init__(self, hamiltonian, locals=None, k_1=None, k_2=None, k_3=None, zone=None):
        super().__init__(hamiltonian, locals=locals, k_1=k_1, k_2=k_2, k_3=k_3, zone=zone)

    def _density(self, pauli=None, n=0, m=None):
        """Calculates the momentum dependent density or spin density.
        Parameters :
        -----------
        pauli : ndarray, optional, default (None)
            Spin Pauli matrix of the same shape as hamiltonian.
        n : int, list or tuple
            The band index of the bra vector.
        m : int
            The band index of the ket vector.
        """
        assert isinstance(n, (int, list, tuple))
        if pauli is None:
            pauli = np.eye(self.norbs)
        if isinstance(n, int):
            if m is None:
                m = n
            for _, psi in self.eigen_properties():
                dens = psi.conjugate().T @ pauli @ psi
                yield  dens[n, m]
        else:
            assert m is None
            n = list(n)
            for _, psi in self.eigen_properties():
                dens = np.diag(psi.conjugate().T @ pauli @ psi)
                yield  np.sum(dens[n])

    def __call__(self, pauli=None, n=0, m=None):
        return np.array(list(self._density(pauli=pauli, n=n, m=m)))

class Current(_Kspace):
    def __init__(self, hamiltonian, locals=None, k_1=None, k_2=None, k_3=None, zone=None):
        super().__init__(hamiltonian, locals=locals, k_1=k_1, k_2=k_2, k_3=k_3, zone=zone)

    def _current(self, pauli=None, direction='x', n=0, m=None):
        """Calculates the current polarized along `pauli` and flowing
            in along `direction`.

        Parameters :
        -----------
        pauli : ndarray optional default (None)
            Spin Pauli matrix of the same shape as hamiltonian.
        direction : str
            The direction of flow of the current.
        n : int, list or tuple
            The band index of the bra vector.
        m : int
            The band index of the ket bector.
        """
        assert isinstance(n, (int, list, tuple))
        if pauli is None:
            pauli = ta.eye(self.norbs)
        if isinstance(n, int):
            if m is None:
                m = n
            for (e_psi, spin_curr) in zip(self.eigen_properties(), self.current_operator(pauli, direction=direction)):
                _, psi = e_psi
                curr = psi.conjugate().T @ spin_curr @ psi
                yield  curr[n, m]
        else:
            assert m is None
            n = list(n)
            for (e_psi, spin_curr) in zip(self.eigen_properties(), self.current_operator(pauli, direction=direction)):
                _, psi = e_psi
                curr = np.diag(psi.conjugate().T @ spin_curr @ psi)
                yield  np.sum(curr[n])

    def __call__(self, pauli=None, direction='x', n=0, m=None):
        return np.array(list(self._current(pauli=pauli, direction=direction, n=n, m=m)))    

class BerryCurvature(_Kspace):
    def __init__(self, hamiltonian, locals=None, 
                 k_1=None, k_2=None, k_3=None, zone=None):
        super().__init__(hamiltonian, locals=locals, 
                         k_1=k_1, k_2=k_2, k_3=k_3, zone=zone)

    def _berry_curvature(self, n, pauli=None, alpha='y', beta='x', multiplicity=None):
        r"""Calculates the momentum dependent Bery curvature.
        We consider the general case of a degenerate band structure
        The Berry curvature matrix of the nth band belonging
        to the sub space \Sigma_n reads:
            :math: `\Omega_n^{i,j} =
            - 2 \sum_{m \notin \Sigma_n} \Im {\frac{v_{\alpha}^{(n,i), m} v_{\beta}^{m,(n,j)}}
                {(E_(n,i) - E_m)^2}}`
        with
            :math: `v_{\alpha}^{(n, i) m} = \langle (n,i) v_{\alpha} m \rangle`,
        and
            :math: `v_{\beta}^{m (n, j)} = \langle m v_{\beta} (n,j) \rangle`.
        So, the Berry curvature is given by the trace of :math: `\omega_n{i, j}` over :math: `\Sigma_n`.
        In the case of non-degeneracy, a single diagonal matrix element should be computed.

        Parameters :
        -----------
        n : int
            The band index for which Berry curvature is calculated.
        pauli : ndarray, optional default (None)
            Spin Pauli matrix of the same shape as hamiltonian. If provided,
            the Berry curvature associated to the spin Hall effect (also called spin Berry curvature)
            is calculated.
        alpha, beta : str
            Velocity directions, the two velocities in the Berry curvature formula.
        multiplicity : int
            The degeneracy of the bands, optional, default (None).
            When it is provided find_multiplicity is not called.
        """
        assert isinstance(n, int)
        assert n < self.norbs
        alpha = self.velocity(alpha)
        if pauli is None:
            beta = self.velocity(beta)
        else:
            beta = self.current_operator(pauli, direction=beta)

        if multiplicity is not None:
            assert isinstance(multiplicity, int) and multiplicity > 0
            lamda = multiplicity

        for (v_alpha, v_beta, e_psi) in zip(alpha, beta, self.eigen_properties()):
            en, psi = e_psi

            energy_factor = (en[None, :] - en[:, None]) ** 2
            energy_factor = (np.divide(1, energy_factor, where=(energy_factor != 0)))
            left = psi.conjugate().T @ v_beta @ psi
            right = psi.conjugate().T @ v_alpha @ psi
            # absorb the energy factor to right
            right *= energy_factor
            omega = left @ right
            if multiplicity is None:
                lamda = find_multiplicity(n, en)
            # ensure that n corresponds
            # to the lowest level of the sub space.
            n = renormalize_level(n, en)
            omega = omega[n:n+lamda, n:n+lamda]
            yield - 2 * np.trace(omega).imag

    def __call__(self, n, pauli=None, alpha='y', beta='x', multiplicity=None):
        return np.array(list(self._berry_curvature(n, pauli=pauli, 
                             alpha=alpha, beta=beta, multiplicity=multiplicity)))

def find_multiplicity(n, energies):
    # finds the degeneracy of a given level n
    # within a bunch en of energies
    energies = np.around(energies, decimals=10)
    en = energies[n]
    return collections.Counter(energies)[en]

def renormalize_level(n, energies):
    """Gives the lowest level belonging to the same
    sub space as n. If n is the lowest level
    then it's returned itself."""
    energies = np.around(energies, decimals=10)
    i = n
    try:
        while True:
            i -= 1
            if energies[i] != energies[n]:
                break
        return i+1
    except IndexError:
        assert np.allclose(energies[0], energies[-1])
        return 0

def _momentum_vectors(k_1, k_2, k_3, zone=None):
    """Generates the momentum vectors in a given `zone`.
    If provided, `zone` should be a function
    of the underlying momentum variables.
    """
    k_s = []
    for element in [k_1, k_2, k_3]:
        if element is not None:
            k_s.append(element)
    ksites = it.product(*k_s)

    if zone is None:
        _vectors = list(ksites)
    else:
        assert callable(zone)
        _vectors = [k for k in ksites if zone(*k)]
    return _vectors

def verified(momenta):
    """Policy for the momentum variables:
       - never start with x, y or z or int
       - always end with x, y or z."""
    for momentum in momenta:
        assert not isinstance(momentum[0], int)
        assert momentum[-1] in 'xyz', "each momentum \
                                      should end with x, y or z"
        assert momentum[0] not in 'xyz', "x, y or z can only appear \
                                    at the end of a momentum name "
    return momenta

class Symbolic:
    """Lambdifies hamiltonian and makes symbolic velocities.
    """
    def __init__(self, hamiltonian, locals=None):
        if isinstance(hamiltonian, str):
            self.hamiltonian = Matrix(sympify(hamiltonian, locals))
        else:
            if isinstance(hamiltonian, kwant.builder.Builder):
                locals.update({'e': e})
                hamiltonian = qsymm.builder_to_model(hamiltonian, real_space=True).tosympy()
            self.hamiltonian = hamiltonian.subs(locals)
        self.norbs = self.hamiltonian.shape[0]
        self.atoms = sorted([s.name for s in self.hamiltonian.atoms(Symbol)])
        if isinstance(hamiltonian, kwant.builder.Builder):
            # default qsymm momenta are taken
            self.momenta = self.atoms
        else:
            self.momenta = verified(self.atoms)

    def _lambdify(self):
        return lambdify([*self.momenta], self.hamiltonian)

    def make_velocities(self):
        """Returns a dict whose keys are the space coords
        and values are the corresponding lambdified velocities.
        """
        velocities = {}
        for momentum in self.momenta:
            coord = momentum[-1]
            velocities[coord] = lambdify([*self.atoms], self.hamiltonian.diff(momentum))
        return velocities

def is_str_sympy_or_builder(hamiltonian):
    return isinstance(hamiltonian, (str, sympy.matrices.MatrixBase, kwant.builder.Builder))

def ensure_is_str_sympy_or_builder(hamiltonian):
    if not is_str_sympy_or_builder(hamiltonian):
        raise TypeError('hamiltonian should be either an str, a SymPy expression or an infinite builder')

def k_system(k_1, k_2, zone=None):
    """Helps to plot 2d density, current or BerryCurvature
    on a momentum mesh defined on an auxiliary square lattice delimited by `zone`.
    `k_1`, `k_2` and `zone` should be the same as the ones used
    to compute the physical quantities."""
    syst = kwant.Builder()
    lat = kwant.lattice.square()
    if zone is None:
        zone = lambda k_x, k_y: True
    syst[(lat(i, j) for i in range(len(k_1)) \
                   for j in range(len(k_2)) \
                   if zone(k_1[i], k_2[j]))] = ' '
    return syst.finalized()









